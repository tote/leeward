# Leeward
*A [Reddit](http://www.reddit.com/) Theme*

### Example
Visit [/r/leeward](http://www.reddit.com/r/leeward/) to see the theme in action.

### Install
1. In your subreddit, go to "Moderation Tools" > "edit stylesheet".
2. Paste the contents of "leeward.css" into the textbox.
3. Replace "Leeward" with the name of your subreddit.
4. Download "spreadsheet.png" and upload it using the "Images" form.
5. Click "Save".

### FAQ
**How do I change the text of "readers"?**

To change "readers" to "adherents", add the following code to the end of `leeward.css`:

```
.subscribers > .word {
    display: none;
}
.subscribers > .number::after {
    content: " adherents";
}
```
